
let postItems;
let saveforlaterItems=[];

const getProducts = () => {
	return fetch("http://localhost:3000/products")
	.then((result) => {
		if (result.status == 200) {
			return Promise.resolve(result.json());
		} else {
			return Promise.reject("Unable to retrieve the list");
		}
	}).then(resultMovie => {
		postItems = resultMovie;
		createpostList();
		return postItems;
	}).catch(error => {
		throw new Error(error);
	})
}




function getSaveForLater() {
	return fetch(" http://localhost:3000/saveforLater").then((result) => {
		if (result.status == 200) {
			return Promise.resolve(result.json());
		} else {
			return Promise.reject("Error");
		}
	}).then(result => {
		saveforlaterItems = result;
		savelaterList();
		return result;
	}).catch(error => {
		throw new Error(error);
	})

}


//3
function addpost(id) {
	if (!ispostPresentInFavItems(id)) {
		let movieObject = getpostById(id)
		saveforlaterItems.push(movieObject);
		console.log(saveforlaterItems)
		return fetch("  http://localhost:3000/saveforLater", {
			method: 'POST',
			body: JSON.stringify(movieObject),
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			}
		}).then((result) => {
			if (result.status == 200 || result.status == 201) {
				return Promise.resolve(saveforlaterItems);
			} else {
				return Promise.reject("post is already added to favourites");
			}
		}).then((favMovieResult) => {
			savelaterList();
			return favMovieResult;
		}).catch(err => {
			throw new Error(err);
		})

	} else {
		throw new Error("post is already added to favourites");
	}

}

function ispostPresentInFavItems(selectedMovieId) {
	for (let favmovie in saveforlaterItems) {
		if (selectedMovieId == saveforlaterItems[favmovie].id) {
			return true;
		}
	}
	return false;
}

function getpostById(id) {
	console.log(id);
	for (let movie in postItems) {
		if (id == postItems[movie].id) {
			console.log(postItems[movie])
			return postItems[movie];
		}
	}
}

const createpostList = () => {
	let domMovieList = '';
	postItems.forEach(element => {
		domMovieList = domMovieList + `
		<div id="${element.id}" class="list-group-item d-flex flex-column align-items-center">
		<h6 class="btn btn-outline-warning"> ${element.title}</h6>
        <h6 class="btn btn-dark" style="text-align=">${element.description}</h6>
        <h6 >Price : $${element.price}</h6>  
        <h6 class="btn btn-dark">Rating : ${element.rating}/5 </h6>    
        <h6 >Stock Unit: ${element.stock}</h6>  
        <h6 class="btn btn-dark">${element.category}</h6>  
      
        
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="${element.thumbnail}" class="img-fluid pb-2" alt="Responsive image" >
          </div>
          <div class="carousel-item">
            <img src="${element.thumbnail}" class="img-fluid pb-2" alt="Responsive image" >
          </div>
          <div class="carousel-item">
            <img src="${element.thumbnail}" class="img-fluid pb-2" alt="Responsive image" >
          </div>
        </div>
      </div>

         
		
		<button
		onclick="addpost(${element.id})" type="button" class="btn btn-primary">
		saveforlater
		</button>
		</div>
		`;
	});
	document.getElementById("moviesList").innerHTML = domMovieList;
}

const savelaterList = () => {
	let domFavouriteList = '';
	let childNode = document.getElementById("favouritesList");
	childNode.innerHTML = '';
	saveforlaterItems.forEach(element => {
		domFavouriteList = domFavouriteList + `
		<div id="${element.id}" class="list-group-item d-flex flex-column align-items-center">
    	<h6 class="btn btn-outline-warning"> ${element.title}</h6>
        <h6 class="btn btn-dark" style="text-align=">${element.description}</h6>
        <h6 >Price : $${element.price}</h6>  
        <h6 class="btn btn-dark">Rating : ${element.rating}/5 </h6>    
        <h6 >Stock Unit: ${element.stock}</h6>  
        <h6 class="btn btn-dark">${element.category}</h6>            
        <img src="${element.thumbnail}" class="img-fluid pb-2" alt="Responsive image" > 
		
       
        <button
		onclick="deletelater(${element.id})" type="button" class="btn btn-primary">
		Remove
		</button>
		</div>
		`;
	});
	childNode.innerHTML = domFavouriteList;
}



 getProducts()
getSaveForLater()